#!/usr/bin/python

# Replace <USERNAME_DATABASE>, <USERNAME>, and <PASSWORD> below with your actual DB, user, and password. 
import psycopg2 
import sys 
con = None 

try: 
	con = psycopg2.connect(database='<USERNAME_DATABASE>', user='<USERNAME>', password='<PASSWORD>') 
	cur = con.cursor() 
	update_set = "UPDATE account_move SET partner_id = " 
	update_select = "(SELECT DISTINCT partner_id FROM account_move_line WHERE account_move.id = account_move_line.move_id)" 
	cur.execute(update_set + update_select + " WHERE EXISTS " +update_select)
	cur.execute("COMMIT") 
	cur.close() 
	conn.close() 
except psycopg2.DatabaseError, e: 
	print e.pgcode 
	print e.pgerror sys.exit() 14
	print 'Error %s' % e 
	sys.exit(1) 
 
finally: 
	if con: con.close() 10