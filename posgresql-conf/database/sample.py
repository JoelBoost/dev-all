#!/usr/bin/python

import psycopg2

conn = psycopg2.connect(database="admin", user="postgres", password="admin", host="127.0.0.1", port="5432")
print "Opened database successfully"
con = None

try:
    cur = conn.cursor()
    cur.execute("SELECT id, name from product_template")
    rows = cur.fetchall()
    for row in rows:
        print('%s, %s' % (row[0], row[1]))
        conn.close()

except psycopg2.DatabaseError, e:
    print e.pgcode
    print e.pgerror
    sys.exit()

finally:
    if con: con.close()
