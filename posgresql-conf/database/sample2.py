#!/usr/bin/python
def main():
    # Database Authentification
    conn_string = "host='127.0.0.1' port=5432 dbname='admin' user='postgres' password='admin'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor()

    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)

    spreadsheetId = '1fKiVuWyM07nuxtyVucAvm3bLMZwmX94sBDiY3qqxTNs'
    rangeName = 'Class Data!A2:AO'

    result = service.spreadsheets().values().get(
        spreadsheetId=spreadsheetId, range=rangeName).execute()
    values = result.get('values', [])
    for row in values:
        try:
            cursor.execute("UPDATE product_template SET 
                name=%s 



                WHERE id=%s", [row[24], row[0]])

        except psycopg2.DatabaseError, e:
            print(e)

    # commit the update and close the connection
    conn.commit()
    cursor.close()
    conn.close()
if __name__ == '__main__':
    main()

print('Update   Completed')