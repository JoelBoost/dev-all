# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#    Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2014-2015 Serpent Consulting Services Pvt. Ltd. (<http://www.serpentcs.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models,fields,api
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT
from openerp.exceptions import Warning

class employee_leave(models.Model):

    _name = 'employee.leave'

    @api.model
    def _get_duration(self):

        for emp_levae in self:

            if emp_levae.start_date and emp_levae.end_date:
                print "Start DAte :::::",emp_levae.start_date, type(emp_levae.start_date)
                print "End DAte :::::",emp_levae.end_date, type(emp_levae.end_date)
                st_date_obj = datetime.strptime(emp_levae.start_date, DEFAULT_SERVER_DATE_FORMAT)
                en_date_obj = datetime.strptime(emp_levae.end_date, DEFAULT_SERVER_DATE_FORMAT)
                print "Start DAte Obj :::::",st_date_obj, type(st_date_obj)
                print "End DAte Obj:::::",st_date_obj, type(st_date_obj)
                difference = en_date_obj - st_date_obj
                print "difference",difference
                print "List OF methods :::::::",dir(difference)
                print "Days ::",difference.days
                emp_levae.duration = difference.days
        return True

    employee_id = fields.Many2one('employee.employee.8', "Employee")
    start_date = fields.Date("Start Date")
    end_date = fields.Date("End Date")
    duration = fields.Integer("Duration", compute=_get_duration)
    state = fields.Selection([('draft','New'),('request','In Progress'),('approve','Approved'),('cancel','Cancelled')],
                             string="Status", default="draft")
    text_message = fields.Text("Message of Onchange")

    @api.onchange('employee_id',)
    def check_employee_status(self):
        if not self.employee_id:
            return {}
        if self.employee_id.type == 'employee':
            print "Hello, This is employee Allowed to create leave request"
            self.text_message = "Hello, Th  is is employee Allowed to create leave request"
        else:
            self.text_message = "You are not an employee, so you cannot create leave request"
            self.employee_id = False
            print "You are not employee, so you cannot create leave request"
            raise Warning("You are not an employee, so you cannot create leave request")

    #This method will update the current record status from Draft to Request/In progress
    @api.multi
    def leave_request(self):

        for leave in self:
            leave.write({'state':'request'})
        return True

    @api.multi
    def leave_approve(self):
        for leave in self:
            print "Leave State:::::",leave.state
            print "Leave Duration:::::",leave.start_date

            leave.write({'state':'approve'})
        return True

    @api.multi
    def leave_cancel(self):
        for leave in self:
            leave.state = 'cancel'
            # leave.write({'state':'cancel'})

        cr, uid, context = self.env.args

        print "Current Database Cursor ::::::::::",cr
        print "Logged In user ID :::::::",uid
        print "Context ::::::::::::",context

        return True

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: