# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#    Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2014-2015 Serpent Consulting Services Pvt. Ltd. (<http://www.serpentcs.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _ #v8
from openerp.osv import fields, osv #v7

class WizardWorkingDays(osv.TransientModel):
    _name = 'wiz.working.days'
    _description = 'WorkingDays Calc'
    
    _columns = {
        'days' : fields.float('Working Days', required=True)
    }
    
    _defaults = {
         'days' : 30
    }
    
    def docalc(self, cr, uid, ids, context=None):
        pool_emp = self.pool.get('employee.employee.8')
        for SELF in self.browse(cr, uid, ids, context=context):
            DAYS = SELF.days
            for Emp_record in pool_emp.browse(cr, uid, context.get('active_ids'), context=context):
                CALC = (Emp_record.basic) - ((Emp_record.basic / DAYS) * Emp_record.leaves)
                pool_emp.write(cr, uid, [Emp_record.id], {'testtotal' :CALC})
        
        return True
    #days = fields.Float(string='WorkingDays', default=30)
