# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#    Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2014-2015 Serpent Consulting Services Pvt. Ltd. (<http://www.serpentcs.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning


STATE = [('draft','Draft'),
         ('interviewed','Interview'),
         ('rejected','Rejected'),
         ('recruited','Recruited'),
         ('hired','Employee'),
         ('out','Out')
         ]

class employee_employee_8(models.Model):

    #This method will generate the sequence on button clicked. with fetch the sequence
    #and append with department code
    @api.multi
    def set_emp_sequence(self):
        for emp in self:
            #Adding a record
            #self.write({'ref_ids':[(0,0,{'name':'Jay', 'email':'vora.jay@serpentcs.com'})]})
            #Edit the x2many(write())
            #self.write({'ref_ids':[(1,5,{'contact':'445', 'email':'vora.jay@serpentcs.com'})]})
            #Delete operations
            #self.write({'ref_ids':[(2,5),(3,6)]})
            #Many2many
            #self.write({'hobbies_ids':[(0,0,{'name':'Dancing'})]})
            #update Many2many
            #self.write({'hobbies_ids':[(6,0,[1,3])]})
            next_seq = self.env['ir.sequence'].get('employee.code.sequence')
            next_seq += "/"
            if emp.department_id:
                next_seq += emp.department_id.code
            emp.emp_ID = next_seq
        return True

    #This method was used for generating sequence from default
    def _get_emp_sequence(self):

     #v7 object   # sequence_obj = self.pool.get('ir.sequence')
     #v8   sequence_obj  = self.env['ir.sequence']
        res = {}
        next_sequence = self.env['ir.sequence'].get('employee.code.sequence')
        res.update({'emp_ID':next_sequence})
        return next_sequence
        # emp_data.emp_ID = next_sequence

    _name = 'employee.employee.8'
    _description = 'Emp Details'
    
    @api.one
    @api.depends('basic', 'leaves')
    def _compute_total(self):
        #Computation goes here
        salary = self.basic - ( (self.basic / 30) * self.leaves)
        self.total = salary
        
    total = fields.Float(string='Salary', store=True, readonly=True, compute='_compute_total')
    leaves = fields.Integer(string='Leaves')
    
    name = fields.Char(string='Name', required=True, index=True, translate=True)
    #This is for info. how to generate sequence from default method.
    # emp_ID = fields.Char("Employee ID", default=_get_emp_sequence)
    emp_ID = fields.Char("Employee ID")
    
    
    image = fields.Binary("Image")
    
    age = fields.Integer(string='Age', help='Enter the approx age!')
    active = fields.Boolean(string='Active', default=True)
    basic = fields.Float(string='Basic', oldname='basic_salary')
    bdate = fields.Datetime(string='BirthDate')
    jdate = fields.Date(string='Join Date')
    notes = fields.Text(string='Notes', copy=False, default='Test note')
    template = fields.Html(string='Template')
    gender = fields.Selection([('male','Male'),('female','Female')],"Gender", default="male")
    
    testtotal = fields.Float(string='TestTotal') 
                         
    department_id = fields.Many2one("department.department_emp", "Department", domain="[('code','!=','IT')]")
    ref_ids =  fields.One2many("reference.detail", "employee_id", "References")
    hobbies_ids = fields.Many2many("hobbies.detail", "employee_hobbies_rel", "emp_id","hobbie_id", "Hobbies Info")
    responsible_id = fields.Many2one('res.partner', "Responsible Person")
    email = fields.Char(related='responsible_id.email', string="Resp Email",)
    phone = fields.Char(related='responsible_id.phone', string="RespContact")
    ref = fields.Reference(selection=[('res.partner','Partner'),('res.users','User'),('employee.employee.8','Employee')],string='Reference')
    ldate = fields.Datetime('Leave Date')
    
    state = fields.Selection(STATE, 'State', readonly=True, default='draft')

    ref_link = fields.Char("External Link")
    type = fields.Selection([('trainee','Trainee'),('probation','Probation'),('employee','Employee')], "Employee Type", default="probation")
    
    #Onchange on type
    @api.v7
    def onchange_type(self, cr, uid, ids, type):
        text = 'Test'
        dom = 'test'
        if type == 'trainee':
            text = 'Trainee'
            dom = 'IT'
        elif type == 'probation':
            text = 'Probation'
            dom = 'Sales'
        return {'value' : {'notes': text}, 'domain': {'department_id' : [('name', '=', dom)]} }
    
    @api.onchange('type')
    def changeif(self):
        if self.type== 'training':
            self.notes = 'Ee'
        
    _order = 'name'
    
    def _check_age(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid, ids, context=context):
            if rec.age <= 10 and rec.gender == 'male':
                return False
        return True
    
    
#    @api.constrains('age')
#    def _check_age(self):
#        if self.age <= 10 and self.gender == 'male':
#                return False 
    
    _constraints = [
        (_check_age, 'Come on, grow up!', ['age']),
    ]

#    #v8 methods
#    @api.model
#    def create(self, vals):
#        vals.update({'notes':'sss'})
#        return super(employee_employee_8, self).create(vals)
#
#    @api.multi
#    def write(self, vals):
#        vals.update({'notes':'Jay'})
#        return super(employee_employee_8, self).write(vals)
#    
#    @api.multi
#    def unlink(self):
#        if self.age> 0:
#            raise except_orm('Warning','It is advisable to deactivate the record rather than deleting it!')
#        return super(employee_employee_8, self).unlink()
#    
#    @api.one
#    def copy(self, default=None):
#        if default is None:
#            default = {}
#        default.update({'age':55})
#        return super(employee_employee_8, self).copy(default)
#    
#
#    
#    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
#        #perform special search
#        print "args",args, limit, order, context
#        return super(employee_employee_8, self).search(cr, uid, args=args, offset=offset, limit=limit, order=order,
#            context=context, count=count)
    
class my_dearptment(models.Model):

    _name = 'department.department_emp'
    
    _rec_name = 'code'

    code = fields.Char("Code")
    name = fields.Char("Name")

    #@api.returns('self')
    #def xyz(self,cr, uid, ids, abc):
     #   return [2,3,4]
   
    #@api.returns('self')
    #def xyz(self,abc):
     #   return [2,3,4]
    

class reference_detail(models.Model):

    _name = 'reference.detail'

    employee_id = fields.Many2one("employee.employee.8", "Employee", ondelete="cascade")

    name = fields.Char("Name")
    contact = fields.Char("Contact Number")
    email = fields.Char("Email")


class hobbies_detail(models.Model):

    _name = 'hobbies.detail'

    name = fields.Char("Name")

#inheritance
class resPartner(models.Model):
    _inherit = 'res.partner'
    
    passport_no = fields.Char(string='Passport Info')
    
    def create(self, cr, uid, vals, context=None):
        vals.update({'email':'contact@serpentcs.com'})
        return super(resPartner, self).create(cr, uid, vals, context)

    def write(self, cr, uid, ids, vals, context=None):
        vals.update({'street2':'Siddhraj Zavod'})
        return super(resPartner, self).write(cr, uid, ids, vals, context)
    
    def read(self, cr, uid, ids, fields, context=None, load='_classic_read'):
        res = super(resPartner, self).read(cr, uid, ids, fields, context, load=load)
        return res
    
    def copy(self, cr, uid, id, default=None, context=None):
        res = super(resPartner, self).copy(cr, uid, id, default, context)
        print res
        return res
   
    def unlink(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid, ids, context):
            if rec.phone:
                raise except_orm('Warning','It is advisable to deactivate the record rather than deleting it, You have a working phone number associated!')
        return super(resPartner, self).unlink(cr, uid, ids, context)
    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
