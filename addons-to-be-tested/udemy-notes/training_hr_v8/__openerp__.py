# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#    Serpent Consulting Services Pvt. Ltd.
#    Copyright (C) 2014-2015 Serpent Consulting Services Pvt. Ltd. (<http://www.serpentcs.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
'name':'Human Resource V8',
'author':'Serpent Consulting Services Pvt. Ltd.',
'website':'http://www.serpentcs.com',
'category': 'HR',
'description': """A Human Resource module in V8""",
'depends':['base'],
'data':[
    'security/training_security.xml',
    'security/ir.model.access.csv',
    'wizard/wizard_workingdays_view.xml',
    'hr_view.xml',
    'employee_leave_view.xml',
    'sequence.xml',
    'workflow/workflow.xml',
    'training_hr_report.xml',
    'views/report_emp_8.xml'
],
'installable':True,
'auto_install':False,
'application':True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: