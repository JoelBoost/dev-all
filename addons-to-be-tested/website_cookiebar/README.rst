.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

==============
CookieBar
==============

This module extends the functionality of website to introduce
a nice header bar notifying users about cookies usage on website.

Installation
============

To install this module on private server, you need to:

* Upload module to your addons folder
* Restart odoo service
* Upgrade modules list
* Install as usual

To install this module on SAAS, you need to:

* Activate the developer mode
* Install "Base import module" (technical name: base_import_module) module which adds capability to install external modules
* Click on "Import Module" link in "Apps" menu
* Select downloaded module archive file (.zip) and click "Import Module" button. Thats it, enjoy!

Usage
=====

To configure this module, you need to:

* Go to Website Admin -> CookieBar
* Configure & Save

Contributors
============

* Nedas Zilinskas <nedas.zilinskas@gmail.com>