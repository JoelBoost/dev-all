# -*- coding: utf-8 -*-
# © 2015 Nedas Žilinskas <nedas.zilinskas@gmail.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    "name": "CookieBar",
    "summary": "Informs visitors about storing cookies on their browser",
    "version": "9.0.1.0.0",
    "category": "Website",
    "website": "http://nedaszilinskas.com/",
    "author": "Nedas Žilinskas <nedas.zilinskas@gmail.com>",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "website",
    ],
    "data": [
        "data/x_cookiebar_config.xml",
        "views/assets.xml",
        "views/x_cookiebar_config.xml",
        "views/templates.xml",
    ],
    "images": [
        "static/description/main_screenshot.png",
    ],
    "price": 9.99,
    "currency": "EUR",
}
