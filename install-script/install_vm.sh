#!/bin/bash

#instance=$(date +"%Y-%m-%d %T")
sudo su - odoo -s /bin/bash

gcloud compute --project "floralboost-pro" \
instances create "instance-10" \
--boot-disk-device-name "instance-10" \
--zone "us-central1-b" \
--machine-type "n1-standard-2" \
--subnet "default" \
--no-restart-on-failure \
--maintenance-policy "TERMINATE" \
--preemptible \
--scopes default="https://www.googleapis.com/auth/cloud-platform" \
--tags "http-server","https-server" \
--image "/ubuntu-os-cloud/ubuntu-1604-xenial-v20160610" \
--boot-disk-size "10" \
--boot-disk-type "pd-ssd"



gcloud compute --project "floralboost-pro" \
instances create "instance-6" \
--boot-disk-device-name "instance-6" \
--zone "us-central1-b" \
--machine-type "f1-micro" \
--network "default" \
--maintenance-policy "MIGRATE" \
--scopes default="https://www.googleapis.com/auth/cloud-platform" \
--tags "http-server","https-server" \
--image "/ubuntu-os-cloud/ubuntu-1604-xenial-v20160610" \
--boot-disk-size "10" \
--boot-disk-type "pd-ssd"
