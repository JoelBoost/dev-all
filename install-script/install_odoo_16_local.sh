#!/bin/bash

#--------------------------------------------------
# Update Server
#--------------------------------------------------
echo -e "\n---- Update Server ----"
sudo apt-get update
sudo apt-get upgrade -y

#--------------------------------------------------
# Install PostgreSQL Server
#--------------------------------------------------
echo -e "\n---- Install PostgreSQL Server ----"
sudo apt-get -y install postgresql postgresql-client postgresql-contrib postgresql-server-dev-all || exit

echo -e "\n---- Creating the ODOO PostgreSQL User  ----"
sudo su - postgres -c "createuser -s joel"
sudo -s
sudo -u postgres psql postgres
ALTER ROLE joel WITH PASSWORD 'admin';
\q

#--------------------------------------------------
# Install Dependencies
#--------------------------------------------------
echo -e "\n---- Install tool packages ----"
sudo apt-get install -y wget subversion git bzr bzrtools python-pip gdebi-core || exit

echo -e "\n---- Install python packages ----"
sudo apt-get -y install python-dateutil python-feedparser python-ldap python-libxslt1 python-lxml python-mako \
python-openid python-psycopg2 python-pybabel python-pychart python-pydot python-pyparsing python-reportlab \
python-simplejson python-tz python-vatnumber python-vobject python-webdav python-werkzeug python-xlwt python-yaml \
python-zsi python-docutils python-psutil python-mock python-unittest2 python-jinja2 python-pypdf python-decorator \
python-requests python-passlib python-pil

echo -e "\n---- Install python libraries ----"
sudo pip install gdata psycogreen || exit
# This is for compatibility with Ubuntu 16.04. Will work on 14.04, 15.04 and 16.04
sudo -H pip install suds || exit

echo -e "\n--- Install nodes"
sudo apt-get install node-clean-css -y || exit
sudo apt-get install node-less -y || exit
sudo apt-get install python-gevent -y || exit

echo -e "\n--- Install other required packages"
sudo apt-get install -y fontconfig libxrender1 libjpeg-turbo8 xfonts-base xfonts-75dpi postgresql-server-dev-all \
libsasl2-dev libldap2-dev libxml2-dev libxslt-dev libffi-dev || exit
sudo easy_install pyPdf vatnumber pydot psycogreen suds ofxparse || exit

echo -e "\n---- Install auto-backup dependencies ----"
sudo pip install pysftp || exit

echo -e "\n---- Install label module dependencies ----"
sudo pip install pyBarcode || exit
sudo apt-get install -y libjpeg-dev || exit
sudo pip install -I pillow || exit

#--------------------------------------------------
# Install Wkhtmltopdf if needed
#--------------------------------------------------

echo -e "\n---- Install wkhtml and place shortcuts on correct place for ODOO 9 ----"
sudo wget http://download.gna.org/wkhtmltopdf/0.12/0.12.1/wkhtmltox-0.12.1_linux-trusty-amd64.deb
sudo sudo dpkg -i wkhtmltox-0.12.1_linux-trusty-amd64.deb
sudo ln -s /usr/local/bin/wkhtmltopdf /usr/bin
sudo ln -s /usr/local/bin/wkhtmltoimage /usr/bin

echo -e "\n---- Create ODOO system user ----"
sudo adduser --system --quiet --shell=/bin/bash --home=$OE_HOME --gecos 'ODOO' --group $OE_USER
#The user should also be added to the sudo'ers group.
sudo adduser $OE_USER sudo

echo -e "\n---- Create Log directory ----"
sudo mkdir /home/joel/Desktop/log

#--------------------------------------------------
# Install ODOO
#--------------------------------------------------
echo -e "\n==== Installing ODOO Server ===="
sudo git clone --depth 1 --branch 9.0 https://www.github.com/odoo/odoo /home/joel/Desktop

sudo chown -R joel:joel /home/joel/*

echo -e "* Create server config file"
sudo cp /home/joel/Desktop/odoo/debian/openerp-server.conf /home/joel/Desktop/odoo-server.conf

echo -e "* Change server config file"