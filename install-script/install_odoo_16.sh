#!/bin/bash
################################################################################
# Script for installing Odoo on Ubuntu 14.04, 15.04 and 16.04 (could be used for other version too)
# Author: Yenthe Van Ginneken
#-------------------------------------------------------------------------------
# This script will install Odoo on your Ubuntu 14.04 server. It can install multiple Odoo instances
# in one Ubuntu because of the different xmlrpc_ports
#-------------------------------------------------------------------------------
# Make a new file:
# sudo nano odoo-install.sh
# Place this content in it a      nd then make the file executable:
# sudo chmod +x odoo-install.sh
# Execute the script to install Odoo:
# ./odoo-install
################################################################################

##fixed parameters
#odoo
OE_USER="odoo"
OE_HOME="/$OE_USER"
OE_HOME_EXT="/$OE_USER/${OE_USER}-server"
#The default port where this Odoo instance will run under (provided you use the command -c in the terminal)
#Set to true if you want to install it, false if you don't need it or have it already installed.
INSTALL_WKHTMLTOPDF="True"
#Set the default Odoo port (you still have to use -c /etc/odoo-server.conf for example to use this.)
OE_PORT="8069"
#Choose the Odoo version which you want to install. For example: 9.0, 8.0, 7.0 or saas-6. When using 'trunk' the master version will be installed.
#IMPORTANT! This script contains extra libraries that are specifically needed for Odoo 9.0
OE_VERSION="9.0"
# Set this to True if you want to install Odoo 9 Enterprise!
IS_ENTERPRISE="False"
#set the superadmin password
OE_SUPERADMIN="admin"
OE_CONFIG="${OE_USER}-server"

NGINX_PORT="80"
DOMAIN_NAME="127.0.0.1"

#--------------------------------------------------
# Update Server
#--------------------------------------------------
echo -e "\n---- Update Server ----"
sudo apt-get update
sudo apt-get upgrade -y

#--------------------------------------------------
# Install PostgreSQL Server
#--------------------------------------------------
echo -e "\n---- Install PostgreSQL Server ----"
sudo apt-get -y install postgresql postgresql-client postgresql-contrib postgresql-server-dev-all || exit

echo -e "\n---- Creating the ODOO PostgreSQL User  ----"
sudo su - postgres -c "createuser -s $OE_USER" 2> /dev/null || true

#--------------------------------------------------
# Install Dependencies
#--------------------------------------------------
echo -e "\n---- Install tool packages ----"
sudo apt-get install -y wget subversion git bzr bzrtools python-pip gdebi-core || exit

echo -e "\n---- Install python packages ----"
sudo apt-get -y install python-dateutil python-feedparser python-ldap python-libxslt1 python-lxml python-mako \
python-openid python-psycopg2 python-pybabel python-pychart python-pydot python-pyparsing python-reportlab \
python-simplejson python-tz python-vatnumber python-vobject python-webdav python-werkzeug python-xlwt python-yaml \
python-zsi python-docutils python-psutil python-mock python-unittest2 python-jinja2 python-pypdf python-decorator \
python-requests python-passlib python-pil

echo -e "\n---- Install python libraries ----"
sudo pip install gdata psycogreen || exit
# This is for compatibility with Ubuntu 16.04. Will work on 14.04, 15.04 and 16.04
sudo -H pip install suds || exit

echo -e "\n--- Install nodes"
sudo apt-get install node-clean-css -y || exit
sudo apt-get install node-less -y || exit
sudo apt-get install python-gevent -y || exit

echo -e "\n--- Install other required packages"
sudo apt-get install -y fontconfig libxrender1 libjpeg-turbo8 xfonts-base xfonts-75dpi postgresql-server-dev-all \
libsasl2-dev libldap2-dev libxml2-dev libxslt-dev libffi-dev || exit
sudo easy_install pyPdf vatnumber pydot psycogreen suds ofxparse || exit

echo -e "\n---- Install auto-backup dependencies ----"
sudo pip install pysftp || exit

echo -e "\n---- Install label module dependencies ----"
sudo pip install pyBarcode || exit
sudo apt-get install -y libjpeg-dev || exit
sudo pip install -I pillow || exit

#--------------------------------------------------
# Install Wkhtmltopdf if needed
#--------------------------------------------------
if [ $INSTALL_WKHTMLTOPDF = "True" ]; then
  echo -e "\n---- Install wkhtml and place shortcuts on correct place for ODOO 9 ----"
  sudo wget http://download.gna.org/wkhtmltopdf/0.12/0.12.1/wkhtmltox-0.12.1_linux-trusty-amd64.deb
  sudo sudo dpkg -i wkhtmltox-0.12.1_linux-trusty-amd64.deb
  sudo ln -s /usr/local/bin/wkhtmltopdf /usr/bin
  sudo ln -s /usr/local/bin/wkhtmltoimage /usr/bin
else
  echo "Wkhtmltopdf isn't installed due to the choice of the user!"
fi

echo -e "\n---- Create ODOO system user ----"
sudo adduser --system --quiet --shell=/bin/bash --home=$OE_HOME --gecos 'ODOO' --group $OE_USER
#The user should also be added to the sudo'ers group.
sudo adduser $OE_USER sudo

echo -e "\n---- Create Log directory ----"
sudo mkdir /var/log/$OE_USER
sudo chown $OE_USER:$OE_USER /var/log/$OE_USER

#--------------------------------------------------
# Install ODOO
#--------------------------------------------------
echo -e "\n==== Installing ODOO Server ===="
sudo git clone --depth 1 --branch $OE_VERSION https://www.github.com/odoo/odoo $OE_HOME_EXT/

if [ $IS_ENTERPRISE = "True" ]; then
    # Odoo Enterprise install!
	echo -e "\n--- Create symlink for node"
    sudo ln -s /usr/bin/nodejs /usr/bin/node
	sudo su $OE_USER -c "mkdir $OE_HOME/enterprise"
    sudo su $OE_USER -c "mkdir $OE_HOME/enterprise/addons"

    echo -e "\n---- Adding Enterprise code under $OE_HOME/enterprise/addons ----"
    sudo git clone --depth 1 --branch 9.0 https://www.github.com/odoo/enterprise "$OE_HOME/enterprise/addons"

    echo -e "\n---- Installing Enterprise specific libraries ----"
    sudo apt-get install nodejs npm
    sudo npm install -g less
    sudo npm install -g less-plugin-clean-css
else
    echo -e "\n---- Create custom module directory ----"
   sudo su - $OE_USER -c "gcloud source repos clone custom --project=floralboost-pro"
fi

echo -e "\n---- Setting permissions on home folder ----"
sudo chown -R $OE_USER:$OE_USER $OE_HOME/*

echo -e "* Create server config file"
sudo cp $OE_HOME_EXT/debian/openerp-server.conf /etc/${OE_CONFIG}.conf
sudo chown $OE_USER:$OE_USER /etc/${OE_CONFIG}.conf
sudo chmod 640 /etc/${OE_CONFIG}.conf

echo -e "* Change server config file"
sudo sed -i s/"db_user = .*"/"db_user = $OE_USER"/g /etc/${OE_CONFIG}.conf
sudo sed -i s/"; admin_passwd.*"/"admin_passwd = $OE_SUPERADMIN"/g /etc/${OE_CONFIG}.conf
sudo su root -c "echo 'logfile = /var/log/$OE_USER/$OE_CONFIG$1.log' >> /etc/${OE_CONFIG}.conf"
if [  $IS_ENTERPRISE = "True" ]; then
    sudo su root -c "echo 'addons_path=$OE_HOME/enterprise/addons,$OE_HOME_EXT/addons' >> /etc/${OE_CONFIG}.conf"
else
    sudo su root -c "echo 'addons_path=$OE_HOME_EXT/addons,$OE_HOME/custom/addons' >> /etc/${OE_CONFIG}.conf"
fi

echo -e "* Create startup file"
sudo su root -c "echo '#!/bin/sh' >> $OE_HOME_EXT/start.sh"
sudo su root -c "echo 'sudo -u $OE_USER $OE_HOME_EXT/openerp-server --config=/etc/${OE_CONFIG}.conf' >> $OE_HOME_EXT/start.sh"
sudo chmod 755 $OE_HOME_EXT/start.sh

#--------------------------------------------------
# Adding ODOO as a deamon (initscript)
#--------------------------------------------------

echo -e "* Create init file"
cat <<EOF > ~/$OE_CONFIG
#!/bin/sh
### BEGIN INIT INFO
# Provides: $OE_CONFIG
# Required-Start: \$remote_fs \$syslog
# Required-Stop: \$remote_fs \$syslog
# Should-Start: \$network
# Should-Stop: \$network
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Enterprise Business Applications
# Description: ODOO Business Applications
### END INIT INFO
PATH=/bin:/sbin:/usr/bin
DAEMON=$OE_HOME_EXT/openerp-server
NAME=$OE_CONFIG
DESC=$OE_CONFIG
# Specify the user name (Default: odoo).
USER=$OE_USER
# Specify an alternate config file (Default: /etc/openerp-server.conf).
CONFIGFILE="/etc/${OE_CONFIG}.conf"
# pidfile
PIDFILE=/var/run/\${NAME}.pid
# Additional options that are passed to the Daemon.
DAEMON_OPTS="-c \$CONFIGFILE"
[ -x \$DAEMON ] || exit 0
[ -f \$CONFIGFILE ] || exit 0
checkpid() {
[ -f \$PIDFILE ] || return 1
pid=\`cat \$PIDFILE\`
[ -d /proc/\$pid ] && return 0
return 1
}
case "\${1}" in
start)
echo -n "Starting \${DESC}: "
start-stop-daemon --start --quiet --pidfile \$PIDFILE \
--chuid \$USER --background --make-pidfile \
--exec \$DAEMON -- \$DAEMON_OPTS
echo "\${NAME}."
;;
stop)
echo -n "Stopping \${DESC}: "
start-stop-daemon --stop --quiet --pidfile \$PIDFILE \
--oknodo
echo "\${NAME}."
;;
restart|force-reload)
echo -n "Restarting \${DESC}: "
start-stop-daemon --stop --quiet --pidfile \$PIDFILE \
--oknodo
sleep 1
start-stop-daemon --start --quiet --pidfile \$PIDFILE \
--chuid \$USER --background --make-pidfile \
--exec \$DAEMON -- \$DAEMON_OPTS
echo "\${NAME}."
;;
*)
N=/etc/init.d/\$NAME
echo "Usage: \$NAME {start|stop|restart|force-reload}" >&2
exit 1
;;
esac
exit 0
EOF

echo -e "* Security Init File"
sudo mv ~/$OE_CONFIG /etc/init.d/$OE_CONFIG
sudo chmod 755 /etc/init.d/$OE_CONFIG
sudo chown root: /etc/init.d/$OE_CONFIG

echo -e "* Change default xmlrpc port"
sudo su root -c "echo 'xmlrpc_port = $OE_PORT' >> /etc/${OE_CONFIG}.conf"

echo -e "* Start ODOO on Startup"
sudo update-rc.d $OE_CONFIG defaults

#--------------------------------------------------
# Install Nginx
#--------------------------------------------------

echo -e "\n---- Install Nginx ----"
sudo apt-get install nginx -y || exit

echo -e "n---- Create sites-available file ----"
sudo echo 'server {                                                                             '  > /etc/nginx/sites-available/odoo || exit
sudo echo "    listen        $NGINX_PORT;                                                       " >> /etc/nginx/sites-available/odoo || exit
sudo echo "    server_name $DOMAIN_NAME;                                                        " >> /etc/nginx/sites-available/odoo || exit
sudo echo '    access_log    /var/log/nginx/access.log;                                         ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '    error_log    /var/log/nginx/error.log;                                           ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '    proxy_buffers 16 64k;                                                            ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '    proxy_buffer_size 128k;                                                          ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '                                                                                     ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '    # cache some static data in memory for 60mins.                                   ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '    location ~* ^/(openerp|openobject|web)/static/ {                                 ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '        proxy_cache_valid 200 60m;                                                   ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '        proxy_buffering    on;                                                       ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '        expires 864000;                                                              ' >> /etc/nginx/sites-available/odoo || exit
sudo echo "        proxy_pass    http://127.0.0.1:$OE_PORT;                                     " >> /etc/nginx/sites-available/odoo || exit
sudo echo '    }                                                                                ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '    location / {                                                                     ' >> /etc/nginx/sites-available/odoo || exit
sudo echo "        proxy_pass    http://127.0.0.1:$OE_PORT;                                     " >> /etc/nginx/sites-available/odoo || exit
sudo echo '        proxy_next_upstream error timeout invalid_header http_500 http_502 http_503; ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '        proxy_set_header Host $host;                                                 ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '        proxy_set_header X-Real-IP $remote_addr;                                     ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '        proxy_set_header X-Forward-For $proxy_add_x_forwarded_for;                   ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '        # by default, do not forward anything                                        ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '        proxy_redirect off;                                                          ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '    }                                                                                ' >> /etc/nginx/sites-available/odoo || exit
sudo echo '}                                                                                    ' >> /etc/nginx/sites-available/odoo || exit                                                                                         >> /etc/nginx/sites-available/odoo || exit

echo -e "\n---- Create sites-enabled file ----"
sudo rm /etc/nginx/sites-available/default || exit
sudo rm /etc/nginx/sites-enabled/default || exit
sudo ln -s /etc/nginx/sites-available/odoo /etc/nginx/sites-enabled/odoo || exit
sudo service nginx restart || exit

echo -e "* Starting Odoo Service"
sudo su root -c "/etc/init.d/$OE_CONFIG start"
echo "-----------------------------------------------------------"
echo "Done! The Odoo server is up and running. Specifications:"
echo "Port: $OE_PORT"
echo "Port: $NGINX_PORT"
echo "Port: $DOMAIN_NAME"
echo "User service: $OE_USER"
echo "User PostgreSQL: $OE_USER"
echo "Code location: $OE_USER"
echo "Addons folder: $OE_USER/$OE_CONFIG/addons/"
echo "Start Odoo service: sudo service $OE_CONFIG start"
echo "Stop Odoo service: sudo service $OE_CONFIG stop"
echo "Restart Odoo service: sudo service $OE_CONFIG restart"
echo "-----------------------------------------------------------"