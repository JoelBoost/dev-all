#!/usr/bin/env bash


# print remote version
git remote -v

#Create a new local branch (odoo) from a git@github.com:odoo/odoo.git's remote branch (9.0):

git init
git clone git@github.com:odoo/odoo.git --depth 1 --branch 9.0 --single-branch /home/joel/Desktop/odoo
git checkout 9.0

#Merge changes from git@github.com:odoo/odoo.git's remote branch (9.0) with local branch (odoo):

git fetch git@github.com:odoo/odoo.git
git checkout 9.0
git merge git@github.com:odoo/odoo.git/9.0

#Update git@github.com:odoo/odoo.git's remote branch (9.0) from a local branch (odoo):

git 9.0sh git@github.com:odoo/odoo.git odoo:9.0

#Creating a new branch on a remote uses the same syntax as updating a remote branch. For example, create new remote branch (beta) on git@github.com:odoo/odoo.git from local branch (odoo):

git 9.0sh git@github.com:odoo/odoo.git odoo:beta
Delete remote branch (9.0) from git@github.com:odoo/odoo.git:

git 9.0sh git@github.com:odoo/odoo.git :9.0