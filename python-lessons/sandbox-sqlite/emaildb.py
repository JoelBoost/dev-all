import sqlite3
import re

conn = sqlite3.connect('emaildb.sqlite')
cur = conn.cursor()

regex = '\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}'
regex_org = '@(.+)'

cur.execute('''
DROP TABLE IF EXISTS Counts''')

cur.execute('''
CREATE TABLE Counts (org TEXT, count INTEGER)''')

fname = 'mbox.txt'
if (len(fname) < 1): fname = 'mbox.txt'
fh = open(str(fname))
for line in fh:
    if line.startswith('From: '):
        line = line.strip()
        org = re.findall(regex, line)
        if len(org) > 0:
            org = re.findall(regex_org, line)
            org = str(org)
            org = org[2:-2]

            cur.execute('SELECT count FROM Counts WHERE org = ? ', (org,))
            row = cur.fetchone()
            if row is None:
                cur.execute('''INSERT INTO Counts (org, count)
                        VALUES ( ?, 1 )''', (org,))
            else:
                cur.execute('UPDATE Counts SET count=count+1 WHERE org = ?',
                            (org,))
conn.commit()

sqlstr = 'SELECT org, count FROM Counts ORDER BY count'

print
print "Counts:"
for row in cur.execute(sqlstr):
    print str(row[0]), row[1]

cur.close()
