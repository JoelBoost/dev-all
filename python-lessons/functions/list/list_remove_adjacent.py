def remove_adjacent(nums):
  # +++your code here+++
  # LAB(begin solution)
  result = []
  for num in nums:
    if len(result) == 0 or num != result[-1]:
      result.append(num)
  return result
  # LAB(replace solution)
  # return
  # LAB(end solution)