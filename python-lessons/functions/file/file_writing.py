
file.write("I'm the first line of the file!\n")
file.write("I'm the second line.\n")
file.write("Third line here, boss.\n")
file.close()

my_file = open("output.txt", "r")
print my_file.readline(),
print my_file.readline(),
print my_file.readline(),

my_file.close()


with open('text.txt','r+') as my_file:
    my_file.write('hi this is written using "with" & "as" no need to close()')
if my_file.close():
    my_file.closed
print my_file.closed