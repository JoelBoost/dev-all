#!/usr/bin/python2.7
###
# Tutorial http://chrisalbon.com/python/beautiful_soup_scrape_table.html
##

import urllib
from bs4 import BeautifulSoup


url = 'http://python-data.dr-chuck.net/comments_280639.html'
request = urllib.urlopen(url).read()
soup = BeautifulSoup(request)

tags = soup
for tag in tags:
    # Look at the parts of a tag
    print 'TAG:',tag
    # print 'URL:',tag.get('href', None)
    # print 'Contents:',tag.contents[0]
    # print 'Attrs:',tag.attrs