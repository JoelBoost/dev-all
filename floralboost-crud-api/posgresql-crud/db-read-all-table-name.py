#!/usr/bin/python

import psycopg2

def db_read_table_names():
    conn = psycopg2.connect(database="admin", user="postgres", password="admin", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("""SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'""")
    for table in cur.fetchall():
        print(table)

print db_read_table_names()