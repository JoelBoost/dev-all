#!/usr/bin/python
import psycopg2

def db_read_col_names(self):
    conn = psycopg2.connect(database="admin", user="postgres", password="admin", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("Select * FROM %s" % self)
    column_name = [desc[0] for desc in cur.description]
    conn.close()
    return column_name



print(('account_invoice: '), ' '.join(db_read_col_names('account_invoice')))
print(('account_move: '), ' '.join(db_read_col_names('account_move')))
print(('account_payment: '), ' '.join(db_read_col_names('account_payment')))
print(('crm_lead: '), ' '.join(db_read_col_names('crm_lead')))
print(('crm_lead_tag: '), ' '.join(db_read_col_names('crm_lead_tag')))
print(('delivery_carrier: '), ' '.join(db_read_col_names('delivery_carrier')))
print(('mail_mass_mailing_list: '), ' '.join(db_read_col_names('mail_mass_mailing_list')))
print(('mail_mass_mailing: '), ' '.join(db_read_col_names('mail_mass_mailing')))
print(('mail_mass_mailing_contact: '), ' '.join(db_read_col_names('mail_mass_mailing_contact')))
print(('mail_message: '), ' '.join(db_read_col_names('mail_message')))
print(('mail_template: '), ' '.join(db_read_col_names('mail_template')))
print(('product_template: '), ' '.join(db_read_col_names('product_template')))
print(('res_users: '), ' '.join(db_read_col_names('res_users')))
print(('res_partner: '), ' '.join(db_read_col_names('res_partner')))