#!/usr/bin/python
import psycopg2

def db_read_col_values(table_name):
    for k in psycopg2.extensions.string_types.keys():
        del psycopg2.extensions.string_types[k]
    conn = psycopg2.connect(database="admin", user="joel", password="admin", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM %s" % table_name)
    data = cur.fetchall()
    conn.close()
    return data

values = db_read_col_values("product_template")
for row in values: print row