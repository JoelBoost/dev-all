#!/bin/env python2.7
import httplib2
import os

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
import psycopg2

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_id.json'
APPLICATION_NAME = 'one-click-install'

def get_credentials():
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def main(sheet_id, table_name):
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)

    result = service.spreadsheets().values().get(
        spreadsheetId=sheet_id, range=table_name).execute()
    values_raw = result.get('values', [])

    print len(values_raw) -1

    column_name = []
    for rows in values_raw[0]: column_name.append(rows)

    value = []
    for rows in values_raw: value.append(rows)
    # # todo encrypt connection information
    conn = psycopg2.connect(database="admin", user="joel", password="admin", host="127.0.0.1", port="5432")
    cur = conn.cursor()

    sql_select = "SELECT id FROM %s" % table_name
    cur.execute(sql_select)

    rows = cur.fetchall()
    db_table_len = len(rows)


    for rows in value[1:]:
        count = 0
        column_id = rows[0]
        if column_id <= db_table_len:
            for column_value in rows:
                column_len = len(column_value)
                if column_len > 0:
                    sql = "UPDATE %s SET %s=(%s) WHERE id = (%s)" % (table_name, column_name[count], "%s", "%s")
                    data = (column_value, column_id)
                    cur.execute(sql, data)
                    count += 1
                else:
                    sql = "UPDATE %s SET %s=(%s) WHERE id = (%s)" % (table_name, column_name[count], "%s", "%s")
                    data = (None, column_id)
                    cur.execute(sql, data)
                    count += 1
        else:
            column_len = len(column_value)
            if column_len > 0:
                sql = "INSERT INTO %s (%s) VALUES (%s)" % (table_name, column_name, column_value)
                cur.execute(sql)
                count += 1
            else:
                sql = "INSERT INTO %s (%s) VALUES (%s)" % (table_name, column_name, None)
                cur.execute(sql)
                count += 1



            conn.commit()
            cur.close()
            conn.close()

if __name__ == '__main__':
    main("1pyNKUajrpOTupMIEDk-qCrzxmAIY0G3ieUhF-4euDz4", "product_template" )