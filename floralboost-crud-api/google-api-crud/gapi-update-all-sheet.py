from __future__ import print_function
import httplib2
import os
import psycopg2

from apiclient import discovery
import oauth2client
from oauth2client import file, client, tools
from httplib2 import Http

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_id.json'
APPLICATION_NAME = 'one-click-install'


def get_credentials():

    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

# read the sheet-id, sheet-range and mysql-table from the sheet "Tables"
def read_sheet_table():
    # sheet information
    sheet_id = '1evf1bUhqhV_BoNe3ph55ZZsFjQI7TfyUtZAP3chx-FY'
    rangeName = 'table'

    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    api_request = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)

    result = api_request.spreadsheets().values().get(
        spreadsheetId=sheet_id, range=rangeName).execute()
    values = result.get('values', [])
    if not values:
        print('No data found.')
    else:
        for row in values:
            update_sheet(row[0], row[1], row[2])

def update_sheet(sheet_id, sheet_range, db_table):
    store = file.Storage('storage.json')
    credentials = store.get()
    if not credentials or credentials.invalid:
        flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
        flow = client.flow_from_clientsecrets('client_id.json', SCOPES)
        credentials = tools.run_flow(flow, store, flags)


    api_request = discovery.build('sheets', 'v4', http=credentials.authorize(Http()))
    for k in psycopg2.extensions.string_types.keys():
        del psycopg2.extensions.string_types[k]

    conn = psycopg2.connect(database="admin", user="postgres", password="admin", host="127.0.0.1", port="5432")
    cur = conn.cursor()

    # todo// add the proper colum name in the sheet table.
    cur.execute("SELECT * FROM %s" % db_table)
    data = cur.fetchall()
    conn.close()

    conn = psycopg2.connect(database="admin", user="postgres", password="admin", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("Select * FROM %s" % db_table)
    column_name = [desc[0] for desc in cur.description]
    cur.close()
    conn.close()

    data.insert(0, column_name)

    data = {'values': [row[0:] for row in data]}

    # floralboost-crud-api request to update sheet
    api_request.spreadsheets().values().update(spreadsheetId=sheet_id,
                                               range=sheet_range, body=(data), valueInputOption='RAW').execute()

read_sheet_table()
print("done")