#!/usr/bin/python2.7
from __future__ import print_function
import argparse
import psycopg2

from apiclient import discovery
from httplib2 import Http
from oauth2client import file, client, tools

SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
store = file.Storage('storage.json')
creds = store.get()
if not creds or creds.invalid:
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
    flow = client.flow_from_clientsecrets('client_id.json', SCOPES)
    creds = tools.run_flow(flow, store, flags)

api_request = discovery.build('sheets', 'v4', http=creds.authorize(Http()))

def db_read_col_values(self):
    for k in psycopg2.extensions.string_types.keys():
        del psycopg2.extensions.string_types[k]

    conn = psycopg2.connect(database="admin", user="postgres", password="admin", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM %s" % self)
    data = cur.fetchall()
    conn.close()

    conn = psycopg2.connect(database="admin", user="postgres", password="admin", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("Select * FROM %s" % self)
    column_name = [desc[0] for desc in cur.description]
    conn.close()

    data.insert(0, column_name)

    data = {'values': [row[0:] for row in data]}
    return data

# request configuration
data = db_read_col_values("product_template")
sheet_id = "1dQii0Sx4YiUDeo1F9N2Y5xKC2bjeNM50PCW8ESRWAbQ"
sheet_range = "Sheet1"

# floralboost-crud-api request to update sheet
api_request.spreadsheets().values().update(spreadsheetId=sheet_id,
                                           range=sheet_range, body=(data), valueInputOption='RAW').execute()
