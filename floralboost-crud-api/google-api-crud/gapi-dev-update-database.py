#!/usr/bin/python2.7
from __future__ import print_function
import httplib2
import os

from apiclient import discovery
import oauth2client
from oauth2client import file, client, tools
import psycopg2

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_id.json'
APPLICATION_NAME = 'one-click-install'

sheet_id = "1pyNKUajrpOTupMIEDk-qCrzxmAIY0G3ieUhF-4euDz4"
rangeName = "product_template"
table_name = "product_template"

def get_credentials():

    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def gapi_read_row_values():
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    api_request = discovery.build('sheets', 'v4', http=http,
                                  discoveryServiceUrl=discoveryUrl)
    result = api_request.spreadsheets().values().get(
        spreadsheetId=sheet_id, range=rangeName).execute()
    values = result.get('values', [])
    return values

# request the sheet values
columns_values = gapi_read_row_values()

# isolate the column title
columns_name = []
for row in columns_values[0]:
    columns_name.append(row)

table_value = []
for i in range(len(columns_name)):
    table_value.append('row['+ str(i) +']')
table_value = (", ".join(table_value[1:]) + ', row[0]')

table_column = (u"=%s, ".join(columns_name)+"=%s")
table_column = table_column[7:]
# print(table_column)
# print(table_value)

# db_write_request = ('"UPDATE' +' '+ table_name +' '+ 'SET' +' '+ table_column +' '+ 'WHERE id=%s",' +'[' +' '+ table_value +' '+ ']' )

# print(db_write_request)

conn = psycopg2.connect(database="admin", user="postgres", password="admin", host="127.0.0.1", port="5432")
cur = conn.cursor()

# for row in (columns_values[1:]):
#     cur.execute("UPDATE %s SET %s WHERE ID=%s", [%s] % (table_name, table_column, row[0], table_value))


for row in (columns_values[1:]):
    cur.execute("UPDATE product_template SET warranty=%s, list_price=%s WHERE id=%s",
                [row[1], row[2],
                 row[0]])

conn.commit()
cur.close()
conn.close()
print("done")
